var express = require("express"),
    handlebars = require('express-handlebars'),
    bodyParser = require('body-parser');
 
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
            	"subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            }];
 
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 //Muestra Post
app.get('/posts', function(req, res){
    res.render('posts', { "title": "All Posts", "posts" : posts } );
});
//Mustre ultimo post
app.get('/posts/new', function(req, res){
	console.log(posts[posts.length - 1]);
	var post = posts[posts.length - 1];
    res.render('posts', { "title": "New", "posts" : {post} } );
});
//muestra post por ID
app.get('/posts/:id', function(req, res){
	console.log(req.params.id);
	var post = posts[req.params.id];
	if(req.params.id < posts.length){
		res.render('posts', { "title": "Get Id Succes" , "posts" : {post} } );
		//res.end("OK");
	}else{
	    res.render('posts', { "title": "Get Id Failed"} );
	    //res.end("KO");
	}
});
//edita post por id por get
app.get('/posts/edit', function(req, res){
	console.log(req.query.id);
	if(req.query.id < posts.length){
		posts[req.query.id].subject = req.query.subject;
		posts[req.query.id].description = req.query.description;
		posts[req.query.id].time = req.query.time;
		res.end("OK");
	}
	res.end("KO");
});
//edita post por id por put
app.put("/posts/:id", function(req,res){	
	if(req.params.id < posts.length){
		console.log("PUT");
		posts[req.params.id].subject = req.query.subject;
		posts[req.params.id].description = req.query.description;
		posts[req.params.id].time = req.query.time;
		res.end("OK");
	}
	res.end("KO");
});
//elimina posts por ID
app.delete("/posts/:id", function(req,res){
		if(req.params.id < posts.length){
			posts.splice(req.params.id,1);
			res.send("OK");
		}
		res.send("KO");
});
//Añade un nuevo post
app.post('/posts', function(req, res){
	posts.push(req.body);
    res.end(JSON.stringify(req.body));
});
 
app.listen(8080);